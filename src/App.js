// // import logo from './logo.svg';
// // import cutepokemon from './cutepokemon.jpg';
// import './App.css';
// import PhotoItem from "./components/PhotoItem";
//
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//           <div className="logos">
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 1세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 2세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 3세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 4세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 5세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 6세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 7세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 8세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 9세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 10세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 11세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 12세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 13세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 14세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 15세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 16세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 17세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 18세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 19세</p></div>
//               <div><img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/><p>윈터 20세</p></div>
//           </div>
//           {element}
//           {/*<p>*/}
//           {/*    Edit <code>src/App.js</code> and save to reload.*/}
//           {/*</p>*/}
//           <a
//               className="App-link"
//               href="https://www.youtube.com/"
//               target="_blank"
//               rel="noopener noreferrer"
//           >
//               Learn React
//           </a>
//       </header>
//     </div>
//   );
// }
//
// export default App;
//
// function formatName(user) {
//     return user.firstName + ' ' + user.lastName;
// }
//
// const user = {
//     firstName: 'Harper',
//     lastName: 'Perez'
// };
//
// const element = (
//     <h1>
//     Hello, {formatName(user)}!
//     </h1>
// );
//
// // function getGreeting(user) {
// //     if (user) {
// //         return <h1>Hello, {formatName(user)}!</h1>;
// //     }
// //     return <h1>Hello, Stranger.</h1>;
// // }

import './App.css';
import PhotoItem from "./components/PhotoItem";
// import React from "react";

function App() {
    return (
        <div className="App">
            {/*<img src={process.env.PUBLIC_URL + '/dog.jpg'} className="App-logo" alt="logo"/>*/}
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/dog.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/1.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/2.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/3.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/4.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/5.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem imgSrc={process.env.PUBLIC_URL + '/6.jpg'} imgAlt="logo" itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
            <PhotoItem itemName='몰라요' itemCount={8}/>
        </div>
    );
}

export default App;