import React from "react";

class PhotoItem extends React.Component {
    render() {
        const { imgSrc, imgAlt, itemName, itemCount} = this.props;

        return (
            <div>
                <img src={imgSrc} alt={imgAlt}/>
                <div>이름: {itemName}</div>
                <div>나이: {itemCount}</div>
            </div>
        );
    }
}

export default PhotoItem;